var express = require('express');
var router = express.Router(); // capital R
const mongoClient = require('mongodb').MongoClient;
var kafka = require('../kafka/client');
var ENV_VAR = require('../config_backend/config');



router.post('/message', async (req, res) => {
    //first post message and then retrive, hence async await.
    await post_messages(req,res);
    await get_messages(req,res);
})

post_messages = (req,res) => {
    console.log('inside messages...', req.body);
    mongoClient.connect(ENV_VAR.IP_MONGODB + ENV_VAR.IP_PORT_MONGO, async (err, client) => {
        if (err) {
            console.log("error connecting to mongodb");
        } else {
            console.log("connection successful");
            const db = client.db('homeaway');
            await db.collection('messages').insertOne(
                {
                    from: req.body.from,
                    to: req.body.to,
                    message: req.body.message
                }, (err, result) => {
                    if (err) {
                        console.log("query error");
                        res.writeHead(400, {
                            'Content-Type': 'text/plain'
                        })
                        res.end("Query Error");
                    } else {
                        console.log("query success1");
                        // res.writeHead(200, {
                        //     'Content-Type': 'text/plain'
                        // })
                        // res.end("Successfully saved profile");
                    }
                })
            client.close();
        }
    })
}

get_messages = (req,res) => {
    mongoClient.connect(ENV_VAR.IP_MONGODB + ENV_VAR.IP_PORT_MONGO, async (err, client) => {
        if (err) {
            console.log("error connecting to mongodb");
        } else {
            console.log("connection successful");
            const db = client.db('homeaway');
            await db.collection('messages').find({
            }).toArray()
                .then((result) => {
                    console.log("message details downloaded", result);
                    if (result.length > 0) {
                        res.writeHead(200, {
                            'Content-Type': 'text/plain'
                        })
                        res.end(JSON.stringify(result));
                    } else {
                        res.writeHead(400, {
                            'Content-Type': 'text/plain'
                        })
                        console.log("No details found");
                        res.end("No details found");
                    }
                }), (err) => {
                    console.log("Unable to fetch messages");
                }
            client.close();
        }
    })

}


module.exports = router;